resource "aws_route53_record" "blue" {
  zone_id         = "${var.route53_zone}"
  name            = "${(var.suffix == "") ? var.application : "${var.application}-${var.suffix}"}"
  type            = "CNAME"
  set_identifier  = "${var.application}-blue-${var.blue_version}"
  ttl             = 60
  records         = ["${var.blue_name}.${data.aws_route53_zone.zone.name}"]

  weighted_routing_policy {
    weight = "${var.blue_weight}"
  }

  count = "${var.blue_weight > 0 ? 1 : 0}"
}

resource "aws_route53_record" "green" {
  zone_id         = "${var.route53_zone}"
  name            = "${(var.suffix == "") ? var.application : "${var.application}-${var.suffix}"}"
  type            = "CNAME"
  set_identifier  = "${var.application}-green-${var.green_version}"
  ttl             = 60
  records         = ["${var.green_name}.${data.aws_route53_zone.zone.name}"]

  weighted_routing_policy {
    weight = "${var.green_weight}"
  }

  count = "${var.green_weight > 0 ? 1 : 0}"
}
