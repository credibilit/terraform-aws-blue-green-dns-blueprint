variable "application" {
  type = "string"
  description = "The application name to serve as prefix on DNS record"
}

variable "suffix" {
  type = "string"
  description = "A suffix to be added to the endpoint DNS name"
  default = ""
}

variable "route53_zone" {
  type = "string"
  description = "The route53 zone to create the weighted DNS record"
}

variable "blue_name" {
  type = "string"
  description = "The DNS on blue deploy"
}

variable "blue_version" {
  type = "string"
  description = "The blue version to append on DNS record"
}

variable "blue_weight" {
  type = "string"
  description = "The weight for the DNS record pointing to blue ELB"
}

variable "green_name" {
  type = "string"
  description = "The DNS on green deploy"
}

variable "green_version" {
  type = "string"
  description = "The green version to append on DNS record"
}

variable "green_weight" {
  type = "string"
  description = "The weight for the DNS record pointing to green ELB"
}
