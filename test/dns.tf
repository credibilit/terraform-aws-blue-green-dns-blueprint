module "blue_green_dns_gradual" {
  source = "../"

  application   = "transition-aaa"
  suffix        = "${var.name}"
  route53_zone  = "${module.vpc.private_zone}"

  blue_name     = "${aws_elb.blue.dns_name}"
  blue_version  = "0.0.1"
  blue_weight   = 80

  green_name    = "${aws_elb.green.dns_name}"
  green_version = "0.0.2"
  green_weight  = 20
}


module "pure_green_deploy" {
  source = "../"

  application   = "pure-blue-${var.name}"
  route53_zone  = "${module.vpc.private_zone}"

  blue_name     = "${aws_elb.blue.dns_name}"
  blue_version  = "0.0.1"
  blue_weight   = 100

  green_name    = "${aws_elb.green.dns_name}"
  green_version = "0.0.2"
  green_weight  = 0
}

module "pure_blue_deploy" {
  source = "../"

  application   = "pure-green-${var.name}"
  route53_zone  = "${module.vpc.private_zone}"

  blue_name     = "${aws_elb.blue.dns_name}"
  blue_version  = "0.0.1"
  blue_weight   = 0

  green_name    = "${aws_elb.green.dns_name}"
  green_version = "0.0.2"
  green_weight  = 100
}
