module "elb_security_group" {
  source      = "git::https://bitbucket.org/credibilit/terraform-aws-security-group-blueprint.git?ref=0.0.2"
  name        = "${var.name}"
  description = "Sample security group"
  vpc         = "${module.vpc.vpc}"
}

resource "aws_elb" "blue" {
  name                        = "blue-${var.name}"
  subnets                     = ["${module.vpc.public_subnets}"]
  security_groups             = ["${module.elb_security_group.security_group["id"]}"]
  internal                    = true
  cross_zone_load_balancing   = true

  listener {
    instance_port             = "80"
    instance_protocol         = "http"
    lb_port                   = "80"
    lb_protocol               = "http"
  }

  tags {
    Name        = "blue-${var.name}"
  }
}

resource "aws_route53_record" "blue_alias" {
  zone_id         = "${module.vpc.private_zone}"
  name            = "${var.name}-${var.blue_version}"
  type            = "A"

  alias {
    name                    = "${aws_elb.blue.dns_name}"
    zone_id                 = "${aws_elb.blue.zone_id}"
    evaluate_target_health  = false
  }
}

resource "aws_elb" "green" {
  name                        = "green-${var.name}"
  subnets                     = ["${module.vpc.public_subnets}"]
  security_groups             = ["${module.elb_security_group.security_group["id"]}"]
  internal                    = true
  cross_zone_load_balancing   = true

  listener {
    instance_port             = "80"
    instance_protocol         = "http"
    lb_port                   = "80"
    lb_protocol               = "http"
  }

  tags {
    Name        = "green-${var.name}"
  }
}

resource "aws_route53_record" "green_alias" {
  zone_id         = "${module.vpc.private_zone}"
  name            = "${var.name}-${var.green_version}"
  type            = "A"

  alias {
    name                    = "${aws_elb.green.dns_name}"
    zone_id                 = "${aws_elb.green.zone_id}"
    evaluate_target_health  = false
  }
}
