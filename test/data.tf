variable account {}

data "aws_availability_zones" "azs" {}

variable "name" {
  default = "blue-green-dns-acme"
}

variable "blue_version" {
  default = "0.0.1"
}

variable "green_version" {
  default = "0.0.2"
}
