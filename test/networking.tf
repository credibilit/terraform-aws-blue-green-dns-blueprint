module "vpc" {
  source = "git::ssh://git@bitbucket.org/credibilit/terraform-vpc-blueprint.git?ref=0.1.1"

  account = "${var.account}"
  name = "${var.name}"
  domain_name = "${var.name}.cloud"
  cidr_block = "10.0.0.0/16"
  azs = "${data.aws_availability_zones.azs.names}"
  az_count = 2
  hosted_zone_comment = "An internal hosted zone to test blue green DNS switch"
  public_subnets_cidr_block = [
    "10.0.0.0/24",
    "10.0.1.0/24"
  ]
}
